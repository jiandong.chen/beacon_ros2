#include "beacon_ros2/stereo_odometry_offline.h"

#include "beacon_ros2/util.h"
#include "cv_bridge/cv_bridge.h"

namespace beacon_ros {

/* ##################### StereoOdometryOffline ############################## */
/* ########################################################################## */
StereoOdometryOffline::StereoOdometryOffline(const std::string& path_to_bag,
                                             const std::string& node_name)
    : PohangStereoImageLoader<StereoOdometryOffline>(path_to_bag, node_name) {
    m_left_img_pub =
        this->create_publisher<sensor_msgs::msg::Image>("left_image", 10000);
    m_right_img_pub =
        this->create_publisher<sensor_msgs::msg::Image>("right_image", 10000);
    m_mono_img_pub =
        this->create_publisher<sensor_msgs::msg::Image>("mono_match", 10000);
    m_stereo_img_pub =
        this->create_publisher<sensor_msgs::msg::Image>("stereo_match", 10000);
}

/* ########################################################################## */
void StereoOdometryOffline::runImpl(StereoImageMsg msg) {
    auto left = cv_bridge::toCvCopy(msg.left);
    auto right = cv_bridge::toCvCopy(msg.right);

    m_pipeline.process(left->image,
                       header2Timestamp(left->header),
                       right->image,
                       header2Timestamp(right->header));

    // pub the image result
    auto display_imgs = m_pipeline.getDisplayImage();
    cv_bridge::CvImage display_left{};
    display_left.image = display_imgs.left_image;
    display_left.header = left->header;
    display_left.encoding = left->encoding;
    m_left_img_pub->publish(*display_left.toImageMsg());

    cv_bridge::CvImage display_right{};
    display_right.image = display_imgs.right_image;
    display_right.header = right->header;
    display_right.encoding = right->encoding;
    m_right_img_pub->publish(*display_right.toImageMsg());

    cv_bridge::CvImage display_mono{};
    display_mono.image = display_imgs.monocular_match;
    display_mono.header = left->header;
    display_mono.encoding = left->encoding;
    m_mono_img_pub->publish(*display_mono.toImageMsg());

    cv_bridge::CvImage display_stereo{};
    display_stereo.image = display_imgs.stereo_match;
    display_stereo.header = right->header;
    display_stereo.encoding = right->encoding;
    m_stereo_img_pub->publish(*display_stereo.toImageMsg());
}

/* ########################################################################## */
void StereoOdometryOffline::loadParameter(const beacon::ParameterHandler& ph) {
    m_pipeline.loadParameter(ph);
}

/* ########################################################################## */
void StereoOdometryOffline::init() {
    if (!m_pipeline.init()) {
        throw std::runtime_error(
            "StereoOdometryOffline init the pipeline failed!");
    }
}

};  // namespace beacon_ros
