#include "beacon_module/logger/logger.h"
#include "beacon_ros2/stereo_odometry_offline.h"

using namespace beacon;
using namespace beacon_ros;

int main(int argc, char** argv) {
    rclcpp::init(argc, argv);

    BeaconLogger::enableDebug();

    ParameterHandler ph{"/workspace/beacon/config/base.yaml"};

    auto node = std::make_shared<StereoOdometryOffline>(
        "/workspace/data/pohang00_small/pohang00_small_0.db3");

    node->loadParameter(ph);
    node->init();

    rclcpp::spin(node);
    rclcpp::shutdown();

    return 0;
}
