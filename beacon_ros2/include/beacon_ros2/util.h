#include "beacon/core/timestamp.h"
#include "std_msgs/msg/header.hpp"

namespace beacon_ros {

inline beacon::Timestamp header2Timestamp(const std_msgs::msg::Header& hdr) {
    return beacon::Timestamp{static_cast<std::uint64_t>(hdr.stamp.sec),
                             static_cast<std::uint64_t>(hdr.stamp.nanosec)};
}

};  // namespace beacon_ros
