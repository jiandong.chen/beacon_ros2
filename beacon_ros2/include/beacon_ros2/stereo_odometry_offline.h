#pragma once

#include <string>

#include "beacon_module/pipeline/stereo_pipeline.h"
#include "offline_process.h"
#include "sensor_msgs/msg/image.hpp"

namespace beacon_ros {

class StereoOdometryOffline
    : public PohangStereoImageLoader<StereoOdometryOffline> {
  public:
    StereoOdometryOffline(
        const std::string& path_to_bag,
        const std::string& node_name = "StereoOdometryOffline");

    void loadParameter(const beacon::ParameterHandler& ph);
    void init();

  protected:
    // main func
    friend PohangStereoImageLoader<StereoOdometryOffline>;
    void runImpl(StereoImageMsg msg);

  protected:
    rclcpp::Publisher<sensor_msgs::msg::Image>::SharedPtr m_left_img_pub{};
    rclcpp::Publisher<sensor_msgs::msg::Image>::SharedPtr m_right_img_pub{};
    rclcpp::Publisher<sensor_msgs::msg::Image>::SharedPtr m_mono_img_pub{};
    rclcpp::Publisher<sensor_msgs::msg::Image>::SharedPtr m_stereo_img_pub{};

  protected:
    // beaocn utility
    beacon::StereoPipeline m_pipeline{};

};  // class StereoOdometry

};  // namespace beacon_ros
