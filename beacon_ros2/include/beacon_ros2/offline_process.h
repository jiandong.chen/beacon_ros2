#include <deque>

#include "beacon_module/logger/logger.h"
#include "beacon_ros2_msgs/srv/run_offline_step.hpp"
#include "rclcpp/rclcpp.hpp"
#include "rosbag2_cpp/reader.hpp"
#include "sensor_msgs/msg/compressed_image.hpp"

namespace beacon_ros {

template <typename Derived>
class PohangStereoImageLoader : public rclcpp::Node {
  public:
    struct StereoImageMsg {
        sensor_msgs::msg::CompressedImage left;
        sensor_msgs::msg::CompressedImage right;

    };  // struct StereoImageMsg

  public:
    PohangStereoImageLoader(const std::string& bag_path,
                            const std::string& node_name)
        : rclcpp::Node{node_name} {
        m_logger->info("PohangStereoImageLoader Init name: {}", node_name);
        m_logger->info("PohangStereoImageLoader Openning Bag: {}", bag_path);

        // ros2 init
        m_reader.open(bag_path);
        m_run_service =
            this->create_service<beacon_ros2_msgs::srv::RunOfflineStep>(
                "RunOffline",
                std::bind(&PohangStereoImageLoader::serviceRun,
                          this,
                          std::placeholders::_1,
                          std::placeholders::_2));
    }

    ~PohangStereoImageLoader() {
        m_reader.close();
    }

  protected:
    void loadData() {
        constexpr int batch_size{100};
        constexpr int min_batch_size{10};

        if (!m_reader.has_next()) {
            // no data to load

            m_logger->info("PohangStereoImageLoader Bag has no Data!");
            return;
        }

        if (m_data.size() > min_batch_size) {
            // no need to load

            m_logger->debug("PohangStereoImageLoader Use Buffered Data!");
            return;
        }

        // max num of data loading
        auto load_count = std::max(batch_size - m_left_data.size(),
                                   batch_size - m_right_data.size());

        m_logger->info("PohangStereoImageLoader Load Count: {}", load_count);

        while (m_reader.has_next() && load_count > 0) {
            rosbag2_storage::SerializedBagMessageSharedPtr msg =
                m_reader.read_next();

            if (msg->topic_name == "/stereo_cam/left_img/compressed") {
                rclcpp::SerializedMessage serialized_msg(*msg->serialized_data);

                sensor_msgs::msg::CompressedImage msg{};
                m_serializer.deserialize_message(&serialized_msg, &msg);

                m_left_data.push_back(std::move(msg));

            } else if (msg->topic_name == "/stereo_cam/right_img/compressed") {
                rclcpp::SerializedMessage serialized_msg(*msg->serialized_data);

                sensor_msgs::msg::CompressedImage msg{};
                m_serializer.deserialize_message(&serialized_msg, &msg);

                m_right_data.push_back(std::move(msg));
            }

            if (m_left_data.size() && m_right_data.size()) {
                StereoImageMsg msg;

                msg.left = m_left_data.front();
                m_left_data.pop_front();

                msg.right = m_right_data.front();
                m_right_data.pop_front();

                m_data.push_back(std::move(msg));

                --load_count;
            }
        }
        m_logger->info(
            "PohangStereoImageLoader Load Finished! Stereo Image Buffer Size: "
            "{}",
            m_data.size());
        m_logger->info(
            "PohangStereoImageLoader Load Finished! Left Buffer Size: {}",
            m_left_data.size());
        m_logger->info(
            "PohangStereoImageLoader Load Finished! Right Buffer Size: {}",
            m_right_data.size());
    }

    void run() {
        while (m_step_to_run) {
            // load the data
            loadData();

            if (!m_reader.has_next() && m_step_to_run > m_data.size()) {
                // we dont have enough data
                // set a smaller step
                m_step_to_run = m_data.size();

                m_logger->warn(
                    "PohangStereoImageLoader No more data to run! Reset "
                    "pending step to: {}",
                    m_step_to_run);
            }

            if (m_step_to_run) {
                // still have step to run
                static_cast<Derived*>(this)->runImpl(std::move(m_data.front()));
                m_data.pop_front();

                --m_step_to_run;
                m_logger->debug("PohangStereoImageLoader Pending run: {}",
                                m_step_to_run);
            }
        }
    }

    void serviceRun(
        const std::shared_ptr<beacon_ros2_msgs::srv::RunOfflineStep::Request>
            request,
        std::shared_ptr<beacon_ros2_msgs::srv::RunOfflineStep::Response>
        /* response*/) {
        m_step_to_run = request->step;
        m_logger->info("PohangStereoImageLoader Request Run {} steps!",
                       request->step,
                       m_step_to_run);

        run();
    }

  protected:
    // data
    std::deque<sensor_msgs::msg::CompressedImage> m_left_data{};
    std::deque<sensor_msgs::msg::CompressedImage> m_right_data{};

    std::deque<StereoImageMsg> m_data{};

    std::uint64_t m_step_to_run{};

  public:
    // ros2 utility

    rosbag2_cpp::Reader m_reader{};
    rclcpp::Serialization<sensor_msgs::msg::CompressedImage> m_serializer{};

    rclcpp::Service<beacon_ros2_msgs::srv::RunOfflineStep>::SharedPtr
        m_run_service;

    // beaocn utility

    beacon::BeaconLogger::Ptr m_logger{beacon::BeaconLogger::get()};

    // interface
};  // class PohangStereoImageLoader<T>

};  // namespace beacon_ros
